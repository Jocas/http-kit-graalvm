(ns es-utils.core
  (:gen-class)
  (:require [clojure.java.io :as io]
            [org.httpkit.client :as http])
  (:import [java.net URI]
           [javax.net.ssl SNIHostName SSLEngine SSLParameters]
           [java.io BufferedReader File]
           [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(defn sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setSSLParameters ssl-engine ssl-params)))

(def sni-client (delay (http/make-client {:ssl-configurer sni-configure})))

(defn load-libsunec []
  (when (io/resource "libsunec.so")
    (let [^String libName "libsunec.so"
          ^File tmpDir (.toFile (Files/createTempDirectory "native-libs" (into-array FileAttribute [])))
          ^File nativeLibTmpFile (File. tmpDir libName)]
      (.deleteOnExit tmpDir)
      (.deleteOnExit nativeLibTmpFile)
      (try
        (io/copy (io/input-stream (io/resource libName)) nativeLibTmpFile)
        (System/load (.getAbsolutePath nativeLibTmpFile))
        (catch Exception e
          (println (format "Failed to load libsunec.so native library %s" e)))))))

(defn -main [& args]
  (load-libsunec)
  (println
    @(http/request
       {:method  :get
        :client  @sni-client
        :headers {"Content-Type" "application/json"}
        :url     (or (first args) "http://localhost:9200")})))
