FROM oracle/graalvm-ce:1.0.0-rc12

ENV GRAALVM_HOME=$JAVA_HOME

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN curl -O https://download.clojure.org/install/linux-install-1.10.0.411.sh
RUN chmod +x linux-install-1.10.0.411.sh
RUN ./linux-install-1.10.0.411.sh

COPY deps.edn /usr/src/app/
RUN clojure -R:native-image
COPY . /usr/src/app
RUN mkdir resources/
RUN cp $JAVA_HOME/jre/lib/amd64/libsunec.so resources/

RUN clojure -A:native-image
