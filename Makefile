docker-native-image:
	docker build -f Dockerfile -t registry.gitlab.com/jocas/http-kit-graalvm .
	docker rm build || true
	docker create --name build registry.gitlab.com/jocas/http-kit-graalvm
	docker cp build:/usr/src/app/target/app es-util
