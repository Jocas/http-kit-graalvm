# http-kit-graalvm

A demo project to create a native executable using GraalVM for a Clojure app that uses `http-kit` library and packages the `libsunec.so` inside the native image.

Based on [this demo project](https://gitlab.com/Jocas/http-kit-native).

## TL;DR

```
$ make docker-native-image
$ ./es-util
WARNING: The sunec native library, required by the SunEC provider, could not be loaded. This library is usually shipped as part of the JDK and can be found under <JAVA_HOME>/jre/lib/<platform>/libsunec.so. It is loaded at run time via System.loadLibrary("sunec"), the first time services from SunEC are accessed. To use this provider's services the java.library.path system property needs to be set accordingly to point to a location that contains libsunec.so. Note that if java.library.path is not set it defaults to the current working directory.
{:opts {:method :get, :client #object[org.httpkit.client.HttpClient 0x7e579219 org.httpkit.client.HttpClient], :headers {Content-Type application/json}, :url http://localhost:9200}, :body {
  "name" : "NfeYlmR",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "mkZR34XpRyGRrvNEDVZbQQ",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "eb782d0",
    "build_date" : "2018-06-29T21:59:26.107521Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
, :headers {:content-encoding gzip, :content-length 296, :content-type application/json; charset=UTF-8}, :status 200}
```

## TODO
- [ ] remove the the warning.
